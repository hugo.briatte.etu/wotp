# Makefile principal

FOLDERS=webserver

# Indique toutes les règles qui ne correspondent pas à la création d'un fichier
.PHONY: all $(FOLDERS)

all: $(FOLDERS)

# Lance make dans le répertoire webserver (-C)
webserver:
	make -C webserver
