#ifndef __SIGNAL__H__
#define __SIGNAL__H__

/**
 * Termine propement les processus zombies
 */
void traitement_processus_zombie(int sig);

/**
 * Initialise la prise en compte des signaux CHLD et PIPE
 */
void initialiser_signaux(void);

#endif
