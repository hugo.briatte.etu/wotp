#include "file.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/stat.h>

#define __BUFF_SIZE__ 1024

char * rewrite_target(char * target) {
  char * res = strchr(target, '?');
  if(res != NULL) *res = '\0';
  if(strlen(target) == 1
     && *target == '/') {
    strcpy(target, "/index.html");
  }
  return target;
}

int is_trying_to_go_back(char * target) {
  char * pointer = target + 1;
  int res = 0;
  while(*pointer != '\0') {
    if(strncmp(pointer, "../", 3) == 0) {
      res -= 1;
      if(res < 0)
	return 1;
      else
	pointer += 3;
    } else if(strncmp(pointer, "./", 2) != 0
	      && strncmp(pointer, "//", 2) != 0
	      && *pointer == '/') {
      res += 1;
      pointer += 1;
    } else {
      pointer += 1;
    }
  }
  return 0;
}

file_check check_file(const char * target, const char * document_root) {
  char filePath[__BUFF_SIZE__];
  snprintf(filePath, __BUFF_SIZE__, "%s%s", document_root, target);
  struct stat statData;
  if(stat(filePath, &statData) == -1) {
    perror("stat");
    return NOT_FOUND;
  } else if((statData.st_mode & S_IFMT) != S_IFREG) {
    fprintf(stderr, "Fichier non regulier : %s\n", filePath);
    return NOT_REGULAR;
  } else if(access(filePath, R_OK)) {
    fprintf(stderr, "Fichier non ouvert pour nous : %s\n", filePath);
    return ACCESS_DENIED;
  } else {
    return OK;
  }
}

FILE * open_file(const char * target, const char * document_root) {
  char filePath[__BUFF_SIZE__];
  snprintf(filePath, __BUFF_SIZE__, "%s%s", document_root, target);
  FILE * res = fopen(filePath, "r");
  if(res == NULL)
    perror("fopen");
  return res;
}

int get_file_size(int fd) {
  struct stat buff;
  if (fstat(fd, &buff) == -1) {
    perror("ftstat fail:");
    return -1;
  }
  return buff.st_size;
}

int copy(FILE* in, FILE* out) {
  char buff[1024];
  int readSize;
  while((readSize = fread(buff, 1, 1024, in)) > 0) {
    if(fwrite(buff, 1, readSize, out) == 0) {
      perror("fwrite");
      return -1;
    }
  }
  if(readSize < 0) {
    perror("fread");
    return -1;
  }
  return 0;
}

char * get_mime_type(const char * target, const char * document_root)
{
  char filePath[__BUFF_SIZE__];
  snprintf(filePath, __BUFF_SIZE__, "%s%s", document_root, target);
  int tube[2];
  pipe(tube);
  if(fork() == 0) {
    close(1);
    dup(tube[1]);
    close(0);
    close(tube[0]);
    execlp("file", "file", "-b", "-i", filePath, NULL);
    return NULL;
  } else {
    char * res = malloc(__BUFF_SIZE__);
    close(tube[1]);
    read(tube[0], res, __BUFF_SIZE__);
    char * find = strchr(res, '\n');
    if(find != NULL) *find = '\0';
    close(tube[0]);
    return res;
  }
}


int flush_check(FILE * f) {
  int res;
  if((res = fflush(f)) != 0) {
    perror("flush");
  }
  return res;
}
