#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "socket.h"
#include "http.h"
#include "file.h"
#include "stats.h"
#include "signal.h"

#define WELCOME_MESSAGE "Bienvenue sur Wotp!\n"\
  "Vu qu'on savait pas quoi mettre comme message d'acceuil et qu'en fait, on est pas très créatif\n"\
  "on s'est dit qu'on allait vous rappeller des petites régles d'or pour naviguer sur notre serveur!\n"\
  "1) Si il y'a des bugs, veuillez remplir ce check-up avant de signaler un problème :\n"\
  "\t-Avez vous pensé à redémarrer la box?\n"\
  "\t-Est-ce que votre chat n'aurait pas mangé le cable éthernet?\n"\
  "\t-Est-ce que vous n'utiliseriez pas Internet Explorer 8?\n"\
  "\t-Essayez de désactiver Ad-Block\n"\
  "2) Plaignez vous allégrement sur notre git et on verra ce qu'on fera :D"

#define BUFF_SIZE 10000

/**
 * Verifie si une ligne est vide ou non (rempli de \r\n)
 */
int is_line_empty(char * line) {
  while(*line != '\0'
	&& (*line == '\r'
	    || *line == '\n')) line += 1;
  return *line == '\0';
}

/**
 * Essaie de récuperer d'un fichier une ligne du fichier donnée et la met dans le buffer
 */
char * fgets_or_exit(char * buffer, int size, FILE * stream) {
  if(fgets(buffer, size, stream) == NULL)
    exit(1);
  return buffer;
}

/**
 * Lit un fichier jusqu'a ce que toutes les lignes du header soient données
 */
void skip_headers(FILE* client) {
    char line[BUFF_SIZE];
    while (!is_line_empty(fgets_or_exit(line, BUFF_SIZE, client)));
}

/**
 * Envoie la page des statistiques sur un fichier
 */
void send_stats(FILE* client) {
  char buff[BUFF_SIZE];
  web_stats * stats = get_stats();
  snprintf(
          buff,
          BUFF_SIZE,
          "<html><body><img src=\"https://thumbs.gfycat.com/RashHeftyBluetickcoonhound-size_restricted.gif\"><ul><li>Nombre de connextions : %d</li><li>Nombre de requêtes : %d</li><li>Nombre de 200 : %d</li><li>Nombre de 400 : %d</li><li>Nombre de 403 : %d</li><li>Nombre de 404 : %d</li></ul></body></html>",
          stats->served_connections,
          stats->served_requests,
          stats->ok_200,
          stats->ko_400,
          stats->ko_403,
          stats->ko_404);
  send_response_mime(client, 200, "OK", buff, "text/html");
}

/**
 * Traite un client donné en envoyant le fichier demandé
 */
int traiter_client_file(FILE * file, const char* document_root) {
  char buff[BUFF_SIZE];
  http_request request;

  fgets_or_exit(buff, BUFF_SIZE, file);
  int bad_request =  parse_http_request(buff, &request);
  skip_headers(file);

  if (request.method == HTTP_UNSUPPORTED) {
    send_response(file, 405, "Method Not Allowed", "Method Not Allowed");
  } else if (bad_request == -1 || (request.http_major == 0 && request.http_minor == 0)) {
    send_response(file, 400, "Bad Request", "Bad Request");
    sem_increment(&(get_stats()->ko_400));
  } else if(!check_header_version(&request)) {
    send_response(file, 505, "Version Not Supported", "Version Not Supported");
  } else {
    rewrite_target(request.target);
    if (strcmp(request.target, "/stats") == 0) {
      send_stats(file);
      sem_increment(&(get_stats()->ok_200));
    } else if (is_trying_to_go_back(request.target)) {
      send_response(file, 403, "Forbidden", "Forbidden ressource");
      sem_increment(&(get_stats()->ko_403));
    } else {
      switch(check_file(request.target, document_root)){
      case NOT_FOUND:
	send_response(file, 404, "Not Found", "Not Found");
	sem_increment(&(get_stats()->ko_400));
	break;
      case NOT_REGULAR:
      case ACCESS_DENIED:
	send_response(file, 403, "Forbidden", "Forbidden ressource");
	sem_increment(&(get_stats()->ko_403));
	break;
      default:
	puts("OUVERTURE");
	FILE * requested_resource = open_file(request.target, document_root);
	if(requested_resource == NULL) {
	  send_response(file, 500, "Internal Error", "Internal Error");
	} else {
	  char * res = get_mime_type(request.target, document_root);
	  int file_size = get_file_size(fileno(requested_resource));
	  send_status(file, 200, "OK");
	  fprintf(file, "Connection: close\r\nContent-Length: %d\r\nContent-Type: %s\r\n\r\n", file_size, res);
	  copy(requested_resource, file);
	  fclose(requested_resource);
	  sem_increment(&(get_stats()->ok_200));
	}
	break;
      }
    }
    sem_increment(&(get_stats()->served_connections));
  }

  if(flush_check(file))
    return -1;
    
  return 0;
}

/**
 * Traite un client donné
 */
int traiter_client(int fd, const char* document_root) {
  FILE * file;
  if((file = fdopen(fd, "a+")) == NULL) {
    perror("Fdopen");
    return -1;
  }
  int res = traiter_client_file(file, document_root);
  fclose(file);
  return res;
}

/** Main */
int main(int argc, char ** argv)
{
  if(argc <= 1) {
    fprintf(stderr, "ARGUMENT RACINE MANQUANT");
  } else {

    char * racine = argv[1];
    
    initialiser_signaux();

    int idServ = creer_serveur(8080);
    if(idServ == -1) {
      return -1;
    }

    if(init_stats() == -1)
      return -1;

    int fd;
    while(1) {
      struct sockaddr addr;
      socklen_t socklen;
      fd = accept(idServ, &addr, &socklen);
      if (fd == -1) {
	if (errno == EINTR) {
	  continue;
	}
      }
      if(fork() == 0) {
        traiter_client(fd, racine);
        sem_increment(&(get_stats()->served_requests));
        exit(0);
      } else {
	close(fd);
      }
    }
  }
}
