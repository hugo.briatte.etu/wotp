#include "socket.h"
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>

int creer_serveur(int port)
{
	int socket_serveur;
	
	/* Créé la socket */
	socket_serveur = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_serveur == -1)
	{
		perror("socket_serveur");
		printf("Erreur lors de la création de la socket serveur\n");
		return -1;
	}
	
	/* Réglages de la socket pour le bind à l'interface */
	struct sockaddr_in saddr;
	saddr.sin_family = AF_INET;         /* Socket IPv4 */
	saddr.sin_port = htons(port);       /* Ecoute sur le port 8080 */
	saddr.sin_addr.s_addr = INADDR_ANY; /* Ecoute sur toutes les interfaces */
	
	int optval = 1;
	
	/* Option pour réutiliser tout de suite la socket après l'arrêt du serveur */
	if (setsockopt(socket_serveur, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) == -1)
	{
		perror("Can not set SO_REUSEADDR option");
	}

	/* Bind de la socket à l'interface */
	if (bind(socket_serveur, (struct sockaddr *) &saddr, sizeof(saddr)) == -1)
	{
		perror("bind socket_serveur");
		printf("Erreur lors du bind de la socket serveur sur les interfaces\n");
		return -1;
	}
	
	/* Signal au système que la socket écoute sur l'interface avec n backlog */
	if (listen(socket_serveur, 10) == -1)
	{
		perror("listen socket_serveur");
		printf("Erreur lors du signal au système: listen, pour la socket serveur\n");
		return -1;
	}

	return socket_serveur;
}
