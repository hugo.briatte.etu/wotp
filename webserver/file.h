#ifndef __FILE_H__
#define __FILE_H__

#include <stdio.h>

/**
 * Réecrit le target en enlevant tout ce qu'il y'a après le ? et remplace / par /index.html
 */
char * rewrite_target(char * target);

typedef enum {NOT_FOUND, NOT_REGULAR, ACCESS_DENIED, OK} file_check; 

/**
 * Vérifie si un fichier est ouvrable à la lecture
 */
file_check check_file(const char * target, const char * document_root);

/**
 * Ouvre un fichier
 */
FILE * open_file(const char * target, const char * document_root);

/**
 * Retourne la taille d'un fichier en octets
 */
int get_file_size(int fd);

/**
 * Copie un fichier dans un autre
 */
int copy(FILE* in, FILE* out);

/**
 * Retourne une chaine de caractères à libérer avec free décrivant le mime du fichier
 */
char * get_mime_type(const char * target, const char * document_root);

/**
 * Vérifie si le fichier demandé est au delas de la racine du serveur
 */
int is_trying_to_go_back(char * target);

/**
 * Vide le contenu présent dans un fichier et perror si besoin
 */
int flush_check(FILE * f);

#endif
