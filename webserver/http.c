#include "http.h"

#include <stdio.h>
#include <string.h>
#include "http_parse.h"

void send_status (FILE * client, int code, const char * reason_phrase) {
  fprintf(client, "HTTP/1.1 %i %s\r\n", code, reason_phrase);
}

void send_response (FILE * client, int code, const char * reason_phrase, const char * message_body) {
  send_response_mime(client, code, reason_phrase, message_body, "text/plain");
}

void send_response_mime (FILE * client, int code, const char * reason_phrase, const char * message_body, const char * mime_type) {
  send_status(client, code, reason_phrase);
  fprintf(client, "Connection: close\r\nContent-Length: %ld\r\nContent-Type : %s\r\n\r\n%s", strlen(message_body), mime_type, message_body);
}

int check_header_version(http_request * header) {
  return header->http_major == 2
    || (header->http_major == 1
	&& (header->http_minor == 0
	    || header->http_minor == 1));
}
