#include "signal.h"
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdio.h>

void traitement_processus_zombie(int sig)
{
  int status = sig;
  while (waitpid(-1, &status, WNOHANG) > 0);
}

void initialiser_signaux(void) {

  struct sigaction sa;
  sa.sa_handler = traitement_processus_zombie;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    perror("sigaction(SIGCHILD)");
  }

  if(signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
    perror("SignalError");
  }
}
