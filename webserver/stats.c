#include "stats.h"
#include <stdlib.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <stdio.h>

static web_stats* stats;

int init_stats(void)
{
  stats = (web_stats*) mmap(NULL, sizeof(web_stats), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  stats->served_connections = 0;
  stats->served_requests = 0;
  stats->ok_200 = 0;
  stats->ko_400 = 0;
  stats->ko_403 = 0;
  stats->ko_404 = 0;
  int res = sem_init(&(stats->sem), 1, 1);
  if(res == -1)
    perror("sem_init");
  return res;
}

web_stats* get_stats(void) {
  return stats;
}

int sem_increment(int * toIncrement) {
  if(sem_wait(&(get_stats()->sem)) == -1) {
    perror("sem_wait");
    return -1;
  }
  *toIncrement += 1;
  if(sem_post(&(get_stats()->sem)) == -1) {
    perror("sem_post");
    return -1;
  }
  return 0;
}
