#ifndef __HTTP_H__
#define __HTTP_H__

#include "http_parse.h"
#include <stdio.h>

/**
 * Envoie une réponse donnée sur le fichier
 */
void send_response(FILE * client, int code, const char * reason_phrase,const char * message_body);

/**
 * Envoie la ligne de statut sur le fichier
 */
void send_status(FILE * client , int code , const char * reason_phrase);

/**
 * Envoie une réponse avec le mime en plus
 */
void send_response_mime (FILE * client, int code, const char * reason_phrase, const char * message_body, const char * mime_type);

/**
 * Vérifie si la version de HTTP dans le header est correct
 */
int check_header_version(http_request * header);

#endif
